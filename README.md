
# Insider Backend Assignment
- Match fixture  project for Insider's backend assignment.
- In every 240 seconds, new match will be generated
- In every 5 seconds, there'll be update on running matches(attack and/or score)
- It has one endpoint that handles get requests:
    - For `/nba/:date` Get request endpoint will return 200 with body that has today's matches and status message.

## Technologies and Tools Used
- Go as programming language
- Fiber 2 as web framework
- Gorm for orm tool and sqlite connections
- godotenv for .env config
- gocron for cron jobs
- zap for logging
- Docker
- Docker-compose
- Gitlab CI for CI/CD tasks

## CI/CD
- This repository has only lint, test and build stage. Build stage builds the frontend and pushes it to the docker registry.
- You can see the pipeline from here: https://gitlab.com/gultekinmsg/insider-api/-/pipelines/
- See the pipeline file from `/.gitlab-ci.yml`

## Quick Start
To run backend for at local:
1. Enter to project's root directory and run backend and frontend with docker-compose :
> docker-compose up -d

**Your frontend url will be localhost:8081*

**Your backend url will be localhost:8080*

## Development
You need golang and sqlite database(you can use existing db in root folder)

After setting .env.local file, type `go get` and `make run` in root folder, then application will start from 8080 port. 
You can try with following curl requests;
```bash
curl --location --request GET 'http://127.0.0.1:8080/nba/2022-02-13'
```
You will get similar to following response from successful get request:
```json
{
  "MatchHistory": [
    {
      "ID": 20,
      "CreatedAt": "2022-02-13T16:15:36.190709+03:00",
      "UpdatedAt": "2022-02-13T16:15:36.190709+03:00",
      "DeletedAt": null,
      "StartTime": "2022-02-13T16:15:36.190641+03:00",
      "EndTime": "2022-02-13T16:19:36.190641+03:00",
      "TeamOneAttackCount": 18,
      "TeamTwoAttackCount": 3,
      "TeamOneScore": 21,
      "TeamTwoScore": 9,
      "Team": [
        {
          "ID": 2,
          "CreatedAt": "0001-01-01T00:00:00Z",
          "UpdatedAt": "0001-01-01T00:00:00Z",
          "DeletedAt": null,
          "Name": "YELLOW",
          "Players": [
            {
              "ID": 6,
              "CreatedAt": "0001-01-01T00:00:00Z",
              "UpdatedAt": "0001-01-01T00:00:00Z",
              "DeletedAt": null,
              "Name": "Huseyin",
              "TotalScore": 0,
              "TotalAssist": 0,
              "TeamID": 2,
              "PlayerScore": [
                {
                  "ID": 186,
                  "CreatedAt": "2022-02-13T16:15:36.194403+03:00",
                  "UpdatedAt": "2022-02-13T16:15:36.194403+03:00",
                  "DeletedAt": null,
                  "AttackCount": 3,
                  "Score": 6,
                  "Assist": 0,
                  "PlayerID": 6,
                  "MatchID": 20
                }
              ]
            },
            {
              "ID": 7,
              "CreatedAt": "0001-01-01T00:00:00Z",
              "UpdatedAt": "0001-01-01T00:00:00Z",
              "DeletedAt": null,
              "Name": "Mert",
              "TotalScore": 0,
              "TotalAssist": 0,
              "TeamID": 2,
              "PlayerScore": [
                {
                  "ID": 187,
                  "CreatedAt": "2022-02-13T16:15:36.195337+03:00",
                  "UpdatedAt": "2022-02-13T16:15:36.195337+03:00",
                  "DeletedAt": null,
                  "AttackCount": 1,
                  "Score": 0,
                  "Assist": 0,
                  "PlayerID": 7,
                  "MatchID": 20
                }
              ]
            }
          ]
        }
      ]
    }
  ],
  "Status": "Success"
}
```

### Available make parameters
- **get dependencies:** `go get`
- **generate mocks:** `make generate-mocks`
- **build:** `make build`
- **run:** `make run`
- **unit tests:** `make unit-test`
- **lint check:** `make lint`
- **test coverage:** `make coverage`

## What Can Be Improved
- DB Tests can be added
- Graceful shutdown can be added

## Self Assessment
- It took about 2 days development.
- Project needs some improvements(described above), but it's Good Enough for an assignment in my opinion.

## Links
- Frontend repository: https://gitlab.com/gultekinmsg/nba

## Developers
- Muhammed Said Gültekin (gultekinmsg) - gultekinmsg@gmail.com