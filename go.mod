module insider-api

go 1.17

require (
	github.com/claudiu/gocron v0.0.0-20151103142354-980c96bf412b
	github.com/gofiber/fiber/v2 v2.26.0
	github.com/golang/mock v1.6.0
	github.com/joho/godotenv v1.4.0
	github.com/phayes/freeport v0.0.0-20220201140144-74d24b5ae9f5
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.21.0
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.5
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.32.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
