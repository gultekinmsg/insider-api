package services

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDateFormatter_Successful(t *testing.T) {
	testCases := []struct {
		given  string
		before string
		after  string
	}{
		{"2022-02-11", "2022-02-10", "2022-02-12"},
		{"2022/02/11", "2022-02-10", "2022-02-12"},
		{"2022*02*11", "2022-02-10", "2022-02-12"},
	}
	for _, testCase := range testCases {
		t.Run(testCase.given, func(t *testing.T) {
			before, after, err := dateFormatter(testCase.given)
			assert.NoError(t, err)
			assert.Equal(t, testCase.before, before)
			assert.Equal(t, testCase.after, after)
		})
	}
}
func TestDateFormatter_Failed(t *testing.T) {
	testCases := []struct {
		given  string
		before string
		after  string
	}{
		{"test_test_test", "", ""},
		{"date_date_date", "", ""},
		{"formatter_formatter", "", ""},
	}
	for _, testCase := range testCases {
		t.Run(testCase.given, func(t *testing.T) {
			before, after, err := dateFormatter(testCase.given)
			assert.Error(t, err)
			assert.Equal(t, testCase.before, before)
			assert.Equal(t, testCase.after, after)
		})
	}
}
func TestDateFormatter_Short(t *testing.T) {
	testCases := []struct {
		given  string
		before string
		after  string
	}{
		{"test", "", ""},
		{"date", "", ""},
		{"format", "", ""},
	}
	for _, testCase := range testCases {
		t.Run(testCase.given, func(t *testing.T) {
			before, after, err := dateFormatter(testCase.given)
			assert.NotNil(t, err)
			assert.Equal(t, testCase.before, before)
			assert.Equal(t, testCase.after, after)
		})
	}
}
