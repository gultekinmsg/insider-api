package services

import (
	"errors"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"insider-api/internal/models"
	"insider-api/internal/repository"
	"math/rand"
	"strconv"
	"time"
)

type NbaService interface {
	GetTotalRecords(date string) (models.Response, error)
	CreateMatches() error
	UpdateMatches() error
}
type Service struct {
	repository repository.NbaRepository
	rndService RndServiceI
	logger     *zap.Logger
}

func NewService(repository repository.NbaRepository, rndService RndServiceI, logger *zap.Logger) *Service {
	return &Service{repository: repository, rndService: rndService, logger: logger}
}
func (s *Service) GetTotalRecords(date string) (models.Response, error) {
	before, after, err := dateFormatter(date)
	if err != nil {
		s.logger.Error("Error while formatting date", zap.Error(err))
		return models.Response{}, err
	}
	matches, err := s.repository.GetMatch(before, after)
	if err != nil {
		s.logger.Error("failed to get match history record", zap.Error(err))
		return models.Response{}, err
	}
	var teamInit []models.Team
	var playerInit []models.Player
	for i, i2 := range matches {
		teamIds, err := s.repository.GetTeamIds(i2.ID)
		if err != nil {
			s.logger.Error("failed to get team ids", zap.Error(err))
			return models.Response{}, err
		}
		for i3, id := range teamIds {
			team, err := s.repository.GetTeam(id)
			teamInit = append(teamInit, team)
			if err != nil {
				s.logger.Error("failed to get team", zap.Error(err))
				return models.Response{}, err
			}
			players, err := s.repository.GetPlayers(team.ID)
			if err != nil {
				s.logger.Error("failed to get players", zap.Error(err))
				return models.Response{}, err
			}
			for i4, player := range players {
				playerInit = append(playerInit, player)
				playerScore, err := s.repository.GetPlayerScore(player.ID, i2.ID)
				if err != nil {
					s.logger.Error("failed to get player score", zap.Error(err))
					return models.Response{}, err
				}
				playerInit[i4].PlayerScore = append(playerInit[i4].PlayerScore, playerScore)
			}
			teamInit[i3].Players = playerInit
			playerInit = []models.Player{}
		}
		matches[i].Team = teamInit
		teamInit = []models.Team{}
	}
	return models.Response{MatchHistory: matches, Status: models.Success}, nil
}
func (s *Service) CreateMatches() error {
	mapFixture := createFixture()
	keys := make([]int, 0, len(mapFixture))
	values := make([]int, 0, len(mapFixture))
	for k, v := range mapFixture {
		keys = append(keys, k)
		values = append(values, v)
	}
	for i := 0; i < 3; i++ {
		matchId, err := s.repository.CreateMatchHistoryRecord(models.Match{StartTime: time.Now(), EndTime: time.Now().Add(time.Minute * 4), TeamOneAttackCount: 0, TeamTwoAttackCount: 0, TeamOneScore: 0, TeamTwoScore: 0})
		if err != nil {
			s.logger.Error("Error while creating match history record", zap.Error(err))
			return err
		}
		err = s.repository.CreateMatchTeamsRecord(matchId, uint(keys[i]))
		if err != nil {
			s.logger.Error("Error while creating match teams record", zap.Error(err))
			return err
		}
		err = s.repository.CreateMatchTeamsRecord(matchId, uint(values[i]))
		if err != nil {
			s.logger.Error("Error while creating match teams record", zap.Error(err))
			return err
		}
		firstTeamsPlayerIds, err := s.repository.GetPlayerIds(keys[i])
		if err != nil {
			s.logger.Error("Error while getting player ids", zap.Error(err))
			return err
		}
		secondTeamsPlayerIds, err := s.repository.GetPlayerIds(values[i])
		if err != nil {
			s.logger.Error("Error while getting player ids", zap.Error(err))
			return err
		}
		for _, firstId := range firstTeamsPlayerIds {
			err = s.repository.CreatePlayerScoreRecord(models.PlayerScore{Model: gorm.Model{}, AttackCount: 0, Score: 0, Assist: 0, PlayerID: firstId, MatchID: matchId})
			if err != nil {
				s.logger.Error("Error while creating match players record", zap.Error(err))
				return err
			}
		}
		for _, secondId := range secondTeamsPlayerIds {
			err = s.repository.CreatePlayerScoreRecord(models.PlayerScore{Model: gorm.Model{}, AttackCount: 0, Score: 0, Assist: 0, PlayerID: secondId, MatchID: matchId})
			if err != nil {
				s.logger.Error("Error while creating match players record", zap.Error(err))
				return err
			}
		}
	}
	return nil
}
func (s *Service) UpdateMatches() error {
	playingMatchesIds, err := s.repository.GetPlayingMatchesIds()
	if len(playingMatchesIds) <= 0 {
		s.logger.Info("No playing matches found at: " + time.Now().String())
		return nil
	}
	if err != nil {
		s.logger.Error("Error while getting playing matches ids", zap.Error(err))
		return err
	}
	for i := 0; i < 3; i++ {
		whoAttacked := s.rndService.GetRandInt(2)
		whichPlayerAttacked := s.rndService.GetRandInt(5)
		isAttackSuccessful := s.rndService.GetRandInt(2)
		if whoAttacked == 0 {
			if isAttackSuccessful == 1 {
				err = UpdateRepositoryCalls(s, playingMatchesIds, whichPlayerAttacked, whoAttacked, i, true)
				if err != nil {
					s.logger.Error("Error while updating", zap.Error(err))
					return err
				}
			} else {
				err = UpdateRepositoryCalls(s, playingMatchesIds, whichPlayerAttacked, whoAttacked, i, false)
				if err != nil {
					s.logger.Error("Error while updating", zap.Error(err))
					return err
				}
			}
		} else {
			if isAttackSuccessful == 1 {
				err = UpdateRepositoryCalls(s, playingMatchesIds, whichPlayerAttacked, whoAttacked, i, true)
				if err != nil {
					s.logger.Error("Error while updating", zap.Error(err))
					return err
				}
			} else {
				err = UpdateRepositoryCalls(s, playingMatchesIds, whichPlayerAttacked, whoAttacked, i, false)
				if err != nil {
					s.logger.Error("Error while updating", zap.Error(err))
					return err
				}
			}
		}
	}
	return nil
}
func UpdateRepositoryCalls(s *Service, playingMatchesIds []uint, whichPlayerAttacked, whoAttacked, i int, isScore bool) error {
	err := s.repository.UpdateMatchHistoryRecord(playingMatchesIds[i], isScore, whoAttacked)
	if err != nil {
		s.logger.Error("Error while updating match history record", zap.Error(err))
		return err
	}
	teamIds, err := s.repository.GetTeamIds(playingMatchesIds[i])
	if err != nil {
		s.logger.Error("Error while getting team ids", zap.Error(err))
		return err
	}
	playerIds, err := s.repository.GetPlayerIds(teamIds[whoAttacked])
	if err != nil {
		s.logger.Error("Error while getting player ids", zap.Error(err))
		return err
	}
	err = s.repository.UpdatePlayerScoreRecord(playerIds[whichPlayerAttacked], playingMatchesIds[i], isScore)
	if err != nil {
		s.logger.Error("Error while updating player score record", zap.Error(err))
		return err
	}
	return nil
}
func dateFormatter(date string) (before, after string, err error) {
	if len(date) != 10 {
		return "", "", errors.New("date should be in format YYYY-MM-DD")
	}
	year := date[:4]
	month := date[5:7]
	day := date[8:10]
	intDay, err := strconv.Atoi(day)
	if err != nil {
		return "", "", err
	}
	stringBeforeDay := strconv.Itoa(intDay - 1)
	stringAfterDay := strconv.Itoa(intDay + 1)
	before = year + "-" + month + "-" + stringBeforeDay
	after = year + "-" + month + "-" + stringAfterDay
	return before, after, err
}
func createFixture() map[int]int {
	all := []int{1, 2, 3, 4, 5, 6}
	fixtureMap := make(map[int]int)
	for i := 0; i < 3; i++ {
		rand.Seed(time.Now().UnixNano())
		rnd1 := rand.Intn(len(all))
		key := all[rnd1]
		all = append(all[:rnd1], all[rnd1+1:]...)
		rand.Seed(time.Now().UnixNano())
		rnd2 := rand.Intn(len(all))
		value := all[rnd2]
		all = append(all[:rnd2], all[rnd2+1:]...)
		fixtureMap[key] = value
	}
	return fixtureMap
}
