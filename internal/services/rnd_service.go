package services

import (
	"math/rand"
	"time"
)

type RndServiceI interface {
	GetRandInt(rangeMax int) int
}
type RndServiceStruct struct{}

func NewRndService() *RndServiceStruct {
	return &RndServiceStruct{}
}
func (s *RndServiceStruct) GetRandInt(rangeMax int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(rangeMax)
}
