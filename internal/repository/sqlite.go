package repository

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type SQLite struct {
	DB *gorm.DB
}

func NewSqliteRepository(dbPath string) (*SQLite, error) {
	db, err := gorm.Open(sqlite.Open(dbPath), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return &SQLite{DB: db}, nil
}
