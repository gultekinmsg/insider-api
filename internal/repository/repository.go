package repository

import (
	"go.uber.org/zap"
	"insider-api/internal/models"
	"time"
)

type NbaRepository interface {
	CreatePlayerScoreRecord(playerScoreRecord models.PlayerScore) error
	CreateMatchHistoryRecord(matchHistoryRecord models.Match) (uint, error)
	CreateMatchTeamsRecord(matchId, teamId uint) error
	GetPlayerIds(teamId int) ([]uint, error)
	GetPlayingMatchesIds() ([]uint, error)
	UpdateMatchHistoryRecord(id uint, isScore bool, team int) error
	UpdatePlayerScoreRecord(playerId, matchId uint, isScore bool) error
	GetMatch(before, after string) ([]models.Match, error)
	GetTeamIds(matchId uint) ([]int, error)
	GetTeam(teamId int) (models.Team, error)
	GetPlayers(teamId uint) ([]models.Player, error)
	GetPlayerScore(playerId, matchId uint) (models.PlayerScore, error)
}

type Repository struct {
	SQLite *SQLite
	logger *zap.Logger
}

func NewRepository(sqlite *SQLite, logger *zap.Logger) (*Repository, error) {
	err := sqlite.DB.AutoMigrate(&models.Team{}, &models.Player{}, &models.PlayerScore{}, &models.Match{})
	if err != nil {
		return &Repository{}, err
	}
	return &Repository{SQLite: sqlite, logger: logger}, nil
}
func (r *Repository) CreatePlayerScoreRecord(playerScoreRecord models.PlayerScore) error {
	err := r.SQLite.DB.Create(&playerScoreRecord).Error
	if err != nil {
		r.logger.Error("failed to create player score record", zap.Error(err))
		return err
	}
	return nil
}
func (r *Repository) CreateMatchHistoryRecord(matchHistoryRecord models.Match) (uint, error) {
	err := r.SQLite.DB.Create(&matchHistoryRecord).Error
	if err != nil {
		r.logger.Error("failed to create match history record", zap.Error(err))
		return 1, err
	}
	return matchHistoryRecord.ID, nil
}
func (r *Repository) CreateMatchTeamsRecord(matchId, teamId uint) error {
	err := r.SQLite.DB.Exec("INSERT INTO match_teams (match_id, team_id) VALUES (?, ?)", matchId, teamId).Error
	if err != nil {
		r.logger.Error("failed to create match teams record", zap.Error(err))
		return err
	}
	return nil
}
func (r *Repository) GetPlayerIds(teamId int) ([]uint, error) {
	var playerIds []uint
	err := r.SQLite.DB.Table("players").Select("id").Where("team_id = ?", teamId).Pluck("id", &playerIds).Error
	if err != nil {
		r.logger.Error("failed to get player ids", zap.Error(err))
		return []uint{}, err
	}
	return playerIds, nil
}
func (r *Repository) GetPlayingMatchesIds() ([]uint, error) {
	var matchIds []uint
	err := r.SQLite.DB.Table("matches").Select("id").Where("start_time < ? and end_time > ?", time.Now(), time.Now()).Pluck("id", &matchIds).Error
	if err != nil {
		r.logger.Error("failed to get playing matches ids", zap.Error(err))
		return []uint{}, err
	}
	return matchIds, nil
}
func (r *Repository) UpdatePlayerScoreRecord(playerId, matchId uint, isScore bool) error {
	if isScore {
		err := r.SQLite.DB.Exec("UPDATE player_scores SET attack_count=attack_count+1, score = score+3 WHERE player_id = ? AND match_id = ?", playerId, matchId).Error
		if err != nil {
			r.logger.Error("failed to update player score record", zap.Error(err))
			return err
		}
	} else {
		err := r.SQLite.DB.Exec("UPDATE player_scores SET attack_count=attack_count+1 WHERE player_id = ? AND match_id = ?", playerId, matchId).Error
		if err != nil {
			r.logger.Error("failed to update player score record", zap.Error(err))
			return err
		}
	}
	return nil
}
func (r *Repository) UpdateMatchHistoryRecord(id uint, isScore bool, team int) error {
	if isScore {
		if team == 0 {
			err := r.SQLite.DB.Exec("UPDATE matches SET team_one_attack_count = team_one_attack_count+1, team_one_score = team_one_score+3 WHERE id = ?", id).Error
			if err != nil {
				r.logger.Error("failed to update match history record", zap.Error(err))
				return err
			}
		} else {
			err := r.SQLite.DB.Exec("UPDATE matches SET team_two_attack_count = team_two_attack_count+1, team_two_score = team_two_score+3 WHERE id = ?", id).Error
			if err != nil {
				r.logger.Error("failed to update match history record", zap.Error(err))
				return err
			}
		}
	} else {
		if team == 0 {
			err := r.SQLite.DB.Exec("UPDATE matches SET team_one_attack_count = team_one_attack_count+1 WHERE id = ?", id).Error
			if err != nil {
				r.logger.Error("failed to update match history record", zap.Error(err))
				return err
			}
		} else {
			err := r.SQLite.DB.Exec("UPDATE matches SET team_two_attack_count = team_two_attack_count+1 WHERE id = ?", id).Error
			if err != nil {
				r.logger.Error("failed to update match history record", zap.Error(err))
				return err
			}
		}
	}
	return nil
}
func (r *Repository) GetMatch(before, after string) ([]models.Match, error) {
	var matchRecords []models.Match
	err := r.SQLite.DB.Where(`created_at >= date(?) and created_at <= date(?)`, before, after).Find(&matchRecords).Error
	if err != nil {
		r.logger.Error("failed to get match history record", zap.Error(err))
		return []models.Match{}, err
	}
	return matchRecords, nil
}
func (r *Repository) GetTeamIds(matchId uint) ([]int, error) {
	var teamIds []int
	err := r.SQLite.DB.Table("match_teams").Select("team_id").Where("match_id = ?", matchId).Pluck("team_id", &teamIds).Error
	if err != nil {
		r.logger.Error("failed to get team ids", zap.Error(err))
		return []int{}, err
	}
	return teamIds, nil
}
func (r *Repository) GetTeam(teamId int) (models.Team, error) {
	var team models.Team
	err := r.SQLite.DB.Where("id = ?", teamId).Find(&team).Error
	if err != nil {
		r.logger.Error("failed to get team", zap.Error(err))
		return models.Team{}, err
	}
	return team, nil
}
func (r *Repository) GetPlayers(teamId uint) ([]models.Player, error) {
	var players []models.Player
	err := r.SQLite.DB.Where("team_id = ?", teamId).Find(&players).Error
	if err != nil {
		r.logger.Error("failed to get players", zap.Error(err))
		return []models.Player{}, err
	}
	return players, nil
}
func (r *Repository) GetPlayerScore(playerId, matchId uint) (models.PlayerScore, error) {
	var playerScore models.PlayerScore
	err := r.SQLite.DB.Where("player_id = ? and match_id = ?", playerId, matchId).Find(&playerScore).Error
	if err != nil {
		r.logger.Error("failed to get player score", zap.Error(err))
		return models.PlayerScore{}, err
	}
	return playerScore, nil
}
