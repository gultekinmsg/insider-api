// Code generated by MockGen. DO NOT EDIT.
// Source: internal/handlers/handler.go

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	fiber "github.com/gofiber/fiber/v2"
	gomock "github.com/golang/mock/gomock"
)

// MockNbaHandler is a mock of NbaHandler interface.
type MockNbaHandler struct {
	ctrl     *gomock.Controller
	recorder *MockNbaHandlerMockRecorder
}

// MockNbaHandlerMockRecorder is the mock recorder for MockNbaHandler.
type MockNbaHandlerMockRecorder struct {
	mock *MockNbaHandler
}

// NewMockNbaHandler creates a new mock instance.
func NewMockNbaHandler(ctrl *gomock.Controller) *MockNbaHandler {
	mock := &MockNbaHandler{ctrl: ctrl}
	mock.recorder = &MockNbaHandlerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockNbaHandler) EXPECT() *MockNbaHandlerMockRecorder {
	return m.recorder
}

// GetTotalRecords mocks base method.
func (m *MockNbaHandler) GetTotalRecords(ctx *fiber.Ctx) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTotalRecords", ctx)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetTotalRecords indicates an expected call of GetTotalRecords.
func (mr *MockNbaHandlerMockRecorder) GetTotalRecords(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTotalRecords", reflect.TypeOf((*MockNbaHandler)(nil).GetTotalRecords), ctx)
}

// RegisterRoutes mocks base method.
func (m *MockNbaHandler) RegisterRoutes(app *fiber.App) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "RegisterRoutes", app)
}

// RegisterRoutes indicates an expected call of RegisterRoutes.
func (mr *MockNbaHandlerMockRecorder) RegisterRoutes(app interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RegisterRoutes", reflect.TypeOf((*MockNbaHandler)(nil).RegisterRoutes), app)
}
