package cron_jobs

import (
	"github.com/claudiu/gocron"
	"go.uber.org/zap"
	"insider-api/internal/services"
)

func UpdateCronJob(service *services.Service, logger *zap.Logger) {
	err := service.UpdateMatches()
	if err != nil {
		logger.Error("update matches failed", zap.Error(err))
		gocron.Clear()
	}
}
func CreateCronJob(service *services.Service, logger *zap.Logger) {
	err := service.CreateMatches()
	if err != nil {
		logger.Error("create matches failed", zap.Error(err))
		gocron.Clear()
	}
}
