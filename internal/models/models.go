package models

import (
	"gorm.io/gorm"
	"time"
)

type Team struct {
	gorm.Model
	Name    string
	Players []Player
}
type Player struct {
	gorm.Model
	Name        string
	TotalScore  int
	TotalAssist int
	TeamID      uint
	PlayerScore []PlayerScore
}
type PlayerScore struct {
	gorm.Model
	AttackCount int
	Score       int
	Assist      int
	PlayerID    uint
	MatchID     uint
}
type Match struct {
	gorm.Model
	StartTime          time.Time
	EndTime            time.Time
	TeamOneAttackCount int
	TeamTwoAttackCount int
	TeamOneScore       int
	TeamTwoScore       int
	Team               []Team `gorm:"many2many:match_teams"`
}
type Response struct {
	MatchHistory []Match
	Status       string
}

const Success = "Success"
