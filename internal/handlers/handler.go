package handlers

import (
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
	"insider-api/internal/services"
)

type NbaHandler interface {
	RegisterRoutes(app *fiber.App)
	GetTotalRecords(ctx *fiber.Ctx) error
}
type Handler struct {
	service services.NbaService
	logger  *zap.Logger
}

func NewHandler(service services.NbaService, logger *zap.Logger) *Handler {
	return &Handler{service: service, logger: logger}
}
func (h *Handler) RegisterRoutes(app *fiber.App) {
	app.Get("/nba/:date", h.GetTotalRecords)
}

func (h *Handler) GetTotalRecords(ctx *fiber.Ctx) error {
	date := ctx.Params("date")
	value, err := h.service.GetTotalRecords(date)
	if err != nil {
		h.logger.Error("error getting value", zap.Error(err))
		ctx.Status(fiber.StatusInternalServerError)
		return nil
	}
	err = ctx.Status(fiber.StatusOK).JSON(value)
	if err != nil {
		h.logger.Error("error sending response", zap.Error(err))
		ctx.Status(fiber.StatusInternalServerError)
		return nil
	}
	return nil
}
