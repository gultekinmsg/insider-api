package handlers

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"insider-api/internal/mocks"
	"insider-api/internal/models"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestController_GetTotalRecords_Successful(t *testing.T) {
	var response models.Response
	data := []byte(`{"MatchHistory":[{"ID":1,"CreatedAt":"2022-02-11T20:15:55.376025+03:00","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"StartTime":"0001-01-01T00:00:00Z","EndTime":"0001-01-01T00:00:00Z","TeamOneAttackCount":5,"TeamTwoAttackCount":10,"TeamOneScore":6,"TeamTwoScore":21,"Team":[{"ID":1,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"REDTeam","Players":[{"ID":1,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Muhammed","TotalScore":0,"TotalAssist":0,"TeamID":1,"PlayerScore":[{"ID":1,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":1,"Score":3,"Assist":0,"PlayerID":1,"MatchID":1}]},{"ID":2,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Said","TotalScore":0,"TotalAssist":0,"TeamID":1,"PlayerScore":[{"ID":2,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":2,"Score":3,"Assist":0,"PlayerID":2,"MatchID":1}]},{"ID":3,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Abdullah","TotalScore":0,"TotalAssist":0,"TeamID":1,"PlayerScore":[{"ID":3,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":0,"Score":0,"Assist":1,"PlayerID":3,"MatchID":1}]},{"ID":4,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Mustafa","TotalScore":0,"TotalAssist":0,"TeamID":1,"PlayerScore":[{"ID":4,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":2,"Score":0,"Assist":1,"PlayerID":4,"MatchID":1}]},{"ID":5,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Ali","TotalScore":0,"TotalAssist":0,"TeamID":1,"PlayerScore":[{"ID":5,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":0,"Score":0,"Assist":0,"PlayerID":5,"MatchID":1}]}]},{"ID":2,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"YELLOWTeam","Players":[{"ID":6,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Huseyin","TotalScore":0,"TotalAssist":0,"TeamID":2,"PlayerScore":[{"ID":6,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":0,"Score":0,"Assist":4,"PlayerID":6,"MatchID":1}]},{"ID":7,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Mert","TotalScore":0,"TotalAssist":0,"TeamID":2,"PlayerScore":[{"ID":7,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":0,"Score":0,"Assist":2,"PlayerID":7,"MatchID":1}]},{"ID":8,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Mete","TotalScore":0,"TotalAssist":0,"TeamID":2,"PlayerScore":[{"ID":8,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":2,"Score":3,"Assist":0,"PlayerID":8,"MatchID":1}]},{"ID":9,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Onur","TotalScore":0,"TotalAssist":0,"TeamID":2,"PlayerScore":[{"ID":9,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":2,"Score":6,"Assist":0,"PlayerID":9,"MatchID":1}]},{"ID":10,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Name":"Orcun","TotalScore":0,"TotalAssist":0,"TeamID":2,"PlayerScore":[{"ID":10,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"AttackCount":6,"Score":12,"Assist":1,"PlayerID":10,"MatchID":1}]}]}]}],"Status":"Success"}`)
	err := json.Unmarshal(data, &response)
	assert.Nil(t, err)
	mockServiceController := gomock.NewController(t)
	mockService := mocks.NewMockNbaService(mockServiceController)
	mockServiceController.Finish()
	app := createTestApp()
	NewHandler(mockService, zap.NewNop()).RegisterRoutes(app)
	mockService.EXPECT().GetTotalRecords(gomock.Any()).Return(response, nil)

	req, err := newHttpRequestWithJson(http.MethodGet, "/nba/2022-02-11", nil)
	assert.Nil(t, err)
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assertBodyEqual(t, res.Body, response)
}
func TestController_GetTotalRecords_Failed_NoQuery(t *testing.T) {
	mockServiceController := gomock.NewController(t)
	mockService := mocks.NewMockNbaService(mockServiceController)
	mockServiceController.Finish()
	app := createTestApp()
	NewHandler(mockService, zap.NewNop()).RegisterRoutes(app)
	req, err := newHttpRequestWithJson(http.MethodGet, "/nba/", nil)
	assert.Nil(t, err)
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNotFound, res.StatusCode)
}
func TestController_GetTotalRecords_Failed_Service(t *testing.T) {
	mockServiceController := gomock.NewController(t)
	mockService := mocks.NewMockNbaService(mockServiceController)
	mockServiceController.Finish()
	app := createTestApp()
	NewHandler(mockService, zap.NewNop()).RegisterRoutes(app)
	mockService.EXPECT().GetTotalRecords(gomock.Any()).Return(models.Response{}, errors.New("error"))
	req, err := newHttpRequestWithJson(http.MethodGet, "/nba/2022-02-11", nil)
	assert.Nil(t, err)
	res, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
}

func createTestApp() *fiber.App {
	return fiber.New(fiber.Config{})
}
func newHttpRequestWithJson(method, url string, body interface{}) (*http.Request, error) {
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	req := httptest.NewRequest(method, url, bytes.NewBuffer(jsonBody))
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}
func assertBodyEqual(t *testing.T, responseBody io.Reader, expected interface{}) {
	var actualBody interface{}
	_ = json.NewDecoder(responseBody).Decode(&actualBody)
	expectedBodyJSON, _ := json.Marshal(expected)
	var expectedBody interface{}
	_ = json.Unmarshal(expectedBodyJSON, &expectedBody)
	assert.Equal(t, expectedBody, actualBody)
}
