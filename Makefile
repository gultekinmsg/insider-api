build:
	go build cmd/main.go
run:
	go run ./cmd/main.go
unit-test:
	go test ./... -short
generate-mocks:
	mockgen -source internal/handlers/handler.go -package mocks -destination internal/mocks/mock_handler.go
	mockgen -source internal/services/service.go -package mocks -destination internal/mocks/mock_service.go
	mockgen -source internal/services/rnd_service.go -package mocks -destination internal/mocks/mock_rnd_service.go
	mockgen -source internal/repository/repository.go -package mocks -destination internal/mocks/mock_repository.go
install:
	go mod tidy
lint:
	golangci-lint run
coverage:
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out
	rm coverage.out