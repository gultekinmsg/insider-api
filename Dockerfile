FROM golang:1.17-alpine
ENV GOOS linux
ENV CGO_ENABLED 1
WORKDIR /main
COPY . .
RUN apk add build-base
RUN go mod download
RUN go build cmd/main.go
CMD ./main
