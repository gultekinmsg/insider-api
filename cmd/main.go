package main

import (
	"errors"
	"github.com/claudiu/gocron"
	"github.com/joho/godotenv"
	"go.uber.org/zap"
	"insider-api/internal/cron_jobs"
	"insider-api/internal/handlers"
	"insider-api/internal/repository"
	"insider-api/internal/services"
	"insider-api/pkg/logger"
	"insider-api/pkg/server"
	"log"
	"os"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	zapLogger := logger.New()
	err := godotenv.Load(".env.local")
	if err != nil {
		log.Println(".env.local not found, reading parameters from os env variables")
	}
	dbPath := os.Getenv("DB_PATH")
	if dbPath == "" {
		zapLogger.Error("DB_PATH env variable not set")
		return errors.New("DB_PATH env variable not set")
	}
	port := os.Getenv("PORT")
	if port == "" {
		zapLogger.Error("PORT env variable not set")
		return errors.New("PORT env variable not set")
	}
	sqLiteDB, err := repository.NewSqliteRepository(dbPath)
	if err != nil {
		zapLogger.Error("error creating sqlite repository", zap.Error(err))
		return err
	}
	nbaRepository, err := repository.NewRepository(sqLiteDB, zapLogger)
	if err != nil {
		zapLogger.Error("error migrating repository", zap.Error(err))
		return err
	}
	rndService := services.NewRndService()
	nbaService := services.NewService(nbaRepository, rndService, zapLogger)
	nbaHandler := handlers.NewHandler(nbaService, zapLogger)
	nbaServer := server.NewNbaServer([]handlers.NbaHandler{nbaHandler}, port, zapLogger)
	cronJobs(nbaService, zapLogger)
	nbaServer.Run()
	return nil
}

func cronJobs(service *services.Service, logger *zap.Logger) {
	gocron.Start()
	gocron.Every(5).Seconds().Do(cron_jobs.UpdateCronJob, service, logger)
	gocron.Every(240).Seconds().Do(cron_jobs.CreateCronJob, service, logger)
}
