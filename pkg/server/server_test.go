package server

import (
	"fmt"
	"github.com/phayes/freeport"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"insider-api/internal/handlers"
	"io"
	"net/http"
	"testing"
)

func TestServer_Successful(t *testing.T) {
	freePort, err := freeport.GetFreePort()
	assert.Nil(t, err)
	port := fmt.Sprintf(":%d", freePort)
	s := NewNbaServer([]handlers.NbaHandler{}, port, zap.NewExample())
	go s.Run()
	testEndpointURL := fmt.Sprintf("http://localhost%s/health", port)
	req, err := http.NewRequest(http.MethodGet, testEndpointURL, http.NoBody)
	assert.Nil(t, err)
	res, err := http.DefaultClient.Do(req)
	assert.Nil(t, err)
	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			t.Error(err)
		}
	}(res.Body)
	assert.Equal(t, http.StatusOK, res.StatusCode)
}
func TestServer_Fail_Invalid_Port(t *testing.T) {
	invalidPort := fmt.Sprintf(":%d", -1)
	s := NewNbaServer([]handlers.NbaHandler{}, invalidPort, zap.NewExample())
	assert.Panics(t, func() { s.Run() })
}
