package server

import (
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
	"insider-api/internal/handlers"
)

type NbaServer struct {
	app    *fiber.App
	port   string
	logger *zap.Logger
}

func NewNbaServer(handlers []handlers.NbaHandler, port string, logger *zap.Logger) NbaServer {
	app := fiber.New()
	server := NbaServer{app: app, port: port, logger: logger}
	server.registerRoutes()
	for _, handler := range handlers {
		handler.RegisterRoutes(app)
	}
	return server
}
func (s NbaServer) Run() {
	err := s.app.Listen(s.port)
	if err != nil {
		s.logger.Panic("Failed to start server", zap.Error(err))
	}
}
func (s NbaServer) Stop() {
	err := s.app.Shutdown()
	if err != nil {
		s.logger.Panic("Failed to stop server", zap.Error(err))
	}
}
func (s NbaServer) registerRoutes() {
	s.app.Get("/health", healthCheck)
}
func healthCheck(c *fiber.Ctx) error {
	c.Status(fiber.StatusOK)
	return nil
}
